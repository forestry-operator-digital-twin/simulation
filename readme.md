# Simulation

A [vortex studio](https://www.cm-labs.com/vortex-studio/) intended to be a digital twin using the [operator_twin software scripts](https://gitlab.com/forestry-operator-digital-twin/simulator).

![scene](https://gitlab.com/forestry-operator-digital-twin/simulation/-/raw/main/image/scene.PNG)

## Running

The simulation is executable using `operator_twin.vxscene` . The forest excavator can be control via a simple game controller.

## big_hand_machine

Big hand machine is a modify version of the excavator provided by [vortex studio](https://www.cm-labs.com/vortex-studio/) student version to make it behave like a simplify version of a forest excavator.

![compare](https://gitlab.com/forestry-operator-digital-twin/simulation/-/raw/main/image/compare.PNG)

![big_hand_machine](https://gitlab.com/forestry-operator-digital-twin/simulation/-/raw/main/image/machine.PNG)

![fingers](https://gitlab.com/forestry-operator-digital-twin/simulation/-/raw/main/image/finger.PNG)