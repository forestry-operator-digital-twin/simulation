
def on_simulation_start(extension):
    # Define time constants to use to filter the signals to the actuators
    extension.SwingTC = 1.0
    extension.BoomTC = 0.05
    extension.StickTC = 0.05
    extension.BucketTC = 0.05
    extension.BladeTC = 0.1
    extension.FingerTC = 0.1

    # Define max speeds of actuators
    extension.SwingMaxSpeed = 1.0
    extension.BoomMaxSpeed = 0.25
    extension.StickMaxSpeed = 0.5
    extension.BucketMaxSpeed = 0.75
    extension.BladeMaxSpeed = 1
    extension.FingerMaxSpeed = 1


def post_step(extension):

    # Calculate the desired speeds for the actuators
    throttleFactor = max(extension.inputs.Throttle.value, 0.2)
    swingSignal = extension.inputs.Swing_Signal.value * \
        extension.SwingMaxSpeed * throttleFactor
    boomSignal = extension.inputs.Boom_Signal.value * \
        extension.BoomMaxSpeed * throttleFactor
    stickSignal = extension.inputs.Stick_Signal.value * \
        extension.StickMaxSpeed * throttleFactor
    bucketSignal = extension.inputs.Bucket_Signal.value * \
        extension.BucketMaxSpeed * throttleFactor
    bladeSignal = extension.inputs.Blade_Signal.value * \
        extension.BladeMaxSpeed * throttleFactor

    # Use a low pass filter on the actuator speeds so they are not instantly responsive
    extension.outputs.Swing_Speed.value = lowPassFilter(
        extension, swingSignal, extension.outputs.Swing_Speed.value, extension.SwingTC)
    extension.outputs.Boom_Speed.value = lowPassFilter(
        extension, boomSignal, extension.outputs.Boom_Speed.value, extension.BoomTC)
    extension.outputs.Stick_Speed.value = lowPassFilter(
        extension, stickSignal, extension.outputs.Stick_Speed.value, extension.StickTC)
    extension.outputs.Bucket_Speed.value = lowPassFilter(
        extension, bucketSignal, extension.outputs.Bucket_Speed.value, extension.BucketTC)
    extension.outputs.Hand_Rotation_Speed.value = lowPassFilter(
        extension, bladeSignal, extension.outputs.Hand_Rotation_Speed.value*1.1, extension.BladeTC)

    control_finder(extension)
    control_hand(extension)


# Basic low pass filter function that smooths out signal based on timeConstant.


def lowPassFilter(extension, inputSignal, outputSignal, timeConstant):
    timeStep = extension.getApplicationContext().getSimulationTimeStep()
    value = ((timeStep * inputSignal) + (timeConstant *
             outputSignal)) / (timeStep + timeConstant)
    return value


def control_finder(extension):
    fingerSignalClose = extension.inputs.Close_Finger_Signal.value
    fingerSignalOpen = extension.inputs.Open_Finger_Signal.value

    if fingerSignalOpen:
        extension.outputs.Finger_Position.value = 0.10
    elif fingerSignalClose:
        extension.outputs.Finger_Position.value = 0


def control_hand(extension):
    HandSignalDown = extension.inputs.Hand_Up.value
    HandSignalStill = extension.inputs.Hand_Down.value

    if HandSignalDown:
        extension.outputs.Hand_Speed.value = -1

        
    elif HandSignalStill:
        extension.outputs.Hand_Speed.value = 1
    else:
        extension.outputs.Hand_Speed.value = 0
        extension.outputs.Hand_Position.value = 0
        
